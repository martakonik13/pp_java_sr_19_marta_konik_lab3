// ***************************************************************
//   Zakupy.java
//
//   Wykorzystuje klase Item do stworzenia przedmiotów i dodania ich do koszyka
//   przechowywanego w ArrayList.
// ***************************************************************

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import cs1.Keyboard;

public class Zakupy
{
    public static void main (String[] args) throws IOException
    {
	ArrayList koszyk = new ArrayList();

	Item rzecz;
	Koszyk kosz  = new Koszyk();
	KoszykArray koszA = new KoszykArray();
	Magazyn mag = new Magazyn();
	String nazwaRzeczy;
	double cenaRzeczy;
	int ilosc;

	mag.magazyn();
	 
	
	String kontynuujZakupy = "t";
	do 
	    {
		System.out.print (mag.toString());
		do
		{
			System.out.print ("Podaj nazwe rzeczy: "); 
			nazwaRzeczy = Keyboard.readString();
			//System.out.print ("Podaj cene jednostkowa: ");
			//cenaRzeczy = Keyboard.readDouble();

			//sprawdz czy jest w magazynie
		}while (mag.czyRzeczJestWMag(nazwaRzeczy)==false);

		System.out.print ("Podaj ilosc: ");
		ilosc = Keyboard.readInt();
		if(!mag.czyIloscWystarczy(nazwaRzeczy,ilosc))
			System.out.print ("nie ma wystarczajacej ilosci rzeczy!");
		else
		{
			// *** stworz nowa rzecz i dodaj ja do koszyka
			//rzecz = new Item(nazwaRzeczy,cenaRzeczy,ilosc);
			//	kosz.dodajDoKoszyka(nazwaRzeczy,cenaRzeczy,ilosc);   // tab
			// koszyk.add(rzecz);
			koszA.dodajDoKoszyka(nazwaRzeczy,mag.cenaMagazynu(nazwaRzeczy),ilosc);  //array
		
		}
		
		// *** wypisz zawartosc koszyka z zastosowaniem println
		
		//	System.out.println(kosz.toString());                 // tab
		//koszyk.toString() - pokazuje nawias i przecinki, dlatego w nowej klasie robie toString
		System.out.println(koszA.toString());
		
		System.out.print ("Kontynuowac zakupy (t/n)? ");
		kontynuujZakupy = Keyboard.readString();
	    }
	while (kontynuujZakupy.equals("t"));

    }
}

