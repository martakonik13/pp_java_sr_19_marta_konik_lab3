// **********************************************************************
//   Koszyk.java
//
//   Reprezentuje koszyk zakupowy jako tablica przedmiotow (Items)
// **********************************************************************

//import Item;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class KoszykArray
{

    private int iloscRzeczy;       // ilosc rzeczy w koszyku
    private double cenaCalkowita;  // calkowita cena przedmiotow w koszyku
    private int pojemnosc;         // biezaca pojemnosc koszyka
    private ArrayList  koszyk;

   
    // -----------------------------------------------------------
    // tworzy pusty koszyk o pojemnosci 5 przedmiotow.
    // -----------------------------------------------------------
    public KoszykArray()
    {
	pojemnosc = 1;
	iloscRzeczy = 0;
	cenaCalkowita = 0.0;
	//koszyk = new Item[pojemnosc];
	koszyk = new ArrayList();
    }
    
    // -------------------------------------------------------
    //  Dodaje rzecz do koszyka.
    // -------------------------------------------------------
    public void dodajDoKoszyka(String nazwaRzeczy, double cena, int ilosc)
    {
    	Item rzecz = new Item(nazwaRzeczy,cena,ilosc);
    	//System.out.println("met dodawania");

 //   	if(iloscRzeczy>=koszyk.size())
  //  		powiekszRozmiar();
    	
    	//System.out.println("dodano do koszyka");// + rzecz.toString());
    	koszyk.add(rzecz);
    	iloscRzeczy++;
    	cenaCalkowita+=cena*ilosc;

    }

    // -------------------------------------------------------
    //  Zwraca zawartosc koszyka wraz z podsumowaniem.
    // -------------------------------------------------------
    public String toString()
    {
	NumberFormat fmt = NumberFormat.getCurrencyInstance();

	String zawartosc = "\nKoszyk\n";
	zawartosc += "\nRzecz \tCena jednostkowa\tIlosc\tSuma\n";

	for (int i = 0; i < iloscRzeczy; i++)
	    zawartosc += koszyk.get(i).toString() + "\n";

	zawartosc += "\nTotal Price: " + fmt.format(cenaCalkowita);
	zawartosc += "\n";

	return zawartosc;
    }

/*    // ---------------------------------------------------------
    //  Zwieksza pojemnosc koszyka o 3
    // ---------------------------------------------------------
    private void powiekszRozmiar()
    {
    	ArrayList wiekszyKoszyk =new ArrayList(); new Item[koszyk.size()+3];

    	for(int i =0; i<koszyk.size();i++)
    		wiekszyKoszyk[i]=koszyk.get(i);
    	
    	System.out.println(wiekszyKoszyk.length);
    	koszyk = wiekszyKoszyk;
   // 	System.out.println("zamienilo: " + koszyk.length); 
    }
*/
}

