

import java.awt.EventQueue;
import java.awt.Rectangle;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListModel;

public class aplikacja extends JFrame {

	private JFrame frmZakupy;
	private DefaultListModel listMagazynName = new DefaultListModel();
	private DefaultListModel listKoszykName = new DefaultListModel();
	private ArrayList<Item> nazwy = new ArrayList<Item>();
	private ArrayList<Item> koszyk = new ArrayList<Item>();
	private JTextField nazwaRzeczy;
	private JTextField ilosc;
	String nazwa=" ";
	int ile=0;
	private JTextField txtRazem;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					aplikacja window = new aplikacja();
					window.frmZakupy.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public aplikacja() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frmZakupy = new JFrame();
		frmZakupy.setTitle("Zakupy");
		frmZakupy.setBounds(100, 100, 581, 298);
		frmZakupy.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmZakupy.getContentPane().setLayout(null);
		
		Magazyn mag = new Magazyn();
		
		try {
			nazwy = mag.magazyn();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JLabel lblDostepneRzeczy = new JLabel("Dostepne rzeczy");
		lblDostepneRzeczy.setBounds(29, 11, 168, 14);
		frmZakupy.getContentPane().add(lblDostepneRzeczy);
		
		//wpisanie z magazynu
		listMagazynName.removeAllElements();
		for(Item tmpRzecz: nazwy)
		{
			listMagazynName.addElement(tmpRzecz);
		}
		
		JButton button = new JButton("Dodaj do koszyka");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
					nazwa = nazwaRzeczy.getText();
					if(mag.czyRzeczJestWMag(nazwa)==false){
					infoBox("brak rzeczy w magazynie, wpisz jeszcze raz.","Ups");
					nazwaRzeczy.setText("");
					return;
					}
				
				ile = Integer.parseInt(ilosc.getText());
				
				if(!mag.czyIloscWystarczy(nazwa,ile)){
					infoBox("nie ma wystarczajacej ilosci, wpisz jeszcze raz.","Ups");
					ilosc.setText("");
					nazwaRzeczy.setText("");
				return;
				}
				Item obiekt= new Item(nazwa,mag.cenaMagazynu(nazwa),ile);
//dorobic dodanie do koszyka
				koszyk.add(obiekt);
				listKoszykName.addElement(obiekt);
				nazwy = mag.usunZMag(nazwa,ile);
				listMagazynName.removeAllElements();
				for(Item tmpRzecz: nazwy)
				{
					listMagazynName.addElement(tmpRzecz);
				}
				txtRazem.setText("razem: "+ mag.cenaCalk(koszyk));
				//nazwy.add(rzecz.getText());
				//rzecz.setText("");
			}
		});
		button.setBounds(213, 173, 145, 23);
		frmZakupy.getContentPane().add(button);
		
		JButton btnKoniec = new JButton("Koniec!");
		btnKoniec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnKoniec.setBounds(213, 214, 145, 23);
		frmZakupy.getContentPane().add(btnKoniec);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(28, 36, 161, 201);
		frmZakupy.getContentPane().add(scrollPane);
		
		JList listMagazyn = new JList(listMagazynName);
		scrollPane.setViewportView(listMagazyn);
		
		JLabel lblWpiszCoChcesz = new JLabel("Wpisz co chcesz kupic");
		lblWpiszCoChcesz.setBounds(213, 38, 168, 14);
		frmZakupy.getContentPane().add(lblWpiszCoChcesz);
		
		JLabel lblPodajIlosc = new JLabel("Podaj ilosc");
		lblPodajIlosc.setBounds(213, 102, 138, 14);
		frmZakupy.getContentPane().add(lblPodajIlosc);
		
		JList listKoszyk = new JList(listKoszykName);
		listKoszyk.setBounds(382, 38, 159, 180);
		frmZakupy.getContentPane().add(listKoszyk);
		
		nazwaRzeczy = new JTextField();
		nazwaRzeczy.setBounds(213, 63, 145, 20);
		frmZakupy.getContentPane().add(nazwaRzeczy);
		nazwaRzeczy.setColumns(10);
		
		ilosc = new JTextField();
		ilosc.setBounds(213, 127, 145, 20);
		frmZakupy.getContentPane().add(ilosc);
		ilosc.setColumns(10);
		
		JLabel lblWKoszyku = new JLabel("Rzeczy w koszyku");
		lblWKoszyku.setBounds(382, 11, 168, 14);
		frmZakupy.getContentPane().add(lblWKoszyku);
		
		txtRazem = new JTextField();
		txtRazem.setText("razem: ");
		txtRazem.setBounds(382, 228, 159, 20);
		frmZakupy.getContentPane().add(txtRazem);
		txtRazem.setColumns(10);
		
		
	}
	 public static void infoBox(String infoMessage, String titleBar)
	    {
	        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + titleBar, JOptionPane.INFORMATION_MESSAGE);
	    }
}
