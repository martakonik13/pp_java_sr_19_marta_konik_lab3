


import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Magazyn {

    public ArrayList magazyn;
 
    public Magazyn()
    {
	magazyn = new ArrayList();
    }
    public ArrayList magazyn() throws IOException
    {
    	File file = new File("magazyn.txt");
    	Scanner scanner = new Scanner(file);
    	String data=" ";
    	Item rzecz;
    	String nazwa=" ";
    	int cena = 0, ilosc = 0; 
    	
    	try{
		while((scanner.hasNext())){
			for(int i = 0; i<3; i++){
				nazwa = scanner.next();
				cena = scanner.nextInt();
				ilosc = scanner.nextInt();
				rzecz = new Item(nazwa,cena,ilosc);
				magazyn.add(rzecz);
			}
		}
		
		}catch (NoSuchElementException e){
			// System.out.println("Koniec pliku");
		}
    	//System.out.println(toString());
		scanner.close();
		return magazyn;
    }
    
    public boolean czyRzeczJestWMag(String nazwa)
    {
    	//System.out.println("nazwa"+ nazwa);
    	//System.out.println("rozmMag" + magazyn.size());
    	Item rzecz;
    	for(int i = 0; i<magazyn.size();i++){
    		rzecz = (Item) magazyn.get(i);
    	//	System.out.println(rzecz.getNazwa());

    		if(nazwa.contentEquals(rzecz.getNazwa())){
    			//System.out.println("jest w mag!");
    			return true;
    		}
    	}
    	System.out.println("brak w magazynie danej rzeczy!");
    	return false;
    }
    public boolean czyIloscWystarczy(String nazwa, int ile)
    {
    	Item rzecz;
    	for(int i = 0; i<magazyn.size();i++){
    		rzecz = (Item) magazyn.get(i);
    	//	System.out.println(rzecz.getNazwa() + rzecz.getIlosc() );

    		if(rzecz.getIlosc()>=ile){
    			//	System.out.println("jest w mag!"+ nazwa);

    			//System.out.println("tu:");
    			if(zmniejszIlosc(nazwa,ile)==true)
    				return true;
    			else return false;
    		}
    	}
    	System.out.println("rzecz juz sie skonczyla!");
    	return false;
    }
    public String toString()
    {
	NumberFormat fmt = NumberFormat.getCurrencyInstance();

	String zawartosc = "\nMagazyn\n";
	zawartosc += "\nRzecz\t\tCena jednostkowa\tIlosc\n";

	for (int i = 0; i <magazyn.size(); i++)
	    zawartosc += magazyn.get(i).toString() + "\n";
	
	zawartosc += "\n";

	return zawartosc;
    }
    public boolean zmniejszIlosc(String nazwa, int ile)
    {
    	Item rzecz;
    	int ilosc;
    	for(int i = 0; i<magazyn.size();i++){
    		rzecz = (Item) magazyn.get(i);

    		if(nazwa.contentEquals(rzecz.getNazwa()))//to ta
    		{
    	//		System.out.println("co?" + nazwa);
    			if(rzecz.zmniejsz(ile)==true)
    					return true;
    			else return false;
    		}
    	}
    	return false;
    }
    public double cenaMagazynu(String nazwa)
    {
    	Item rzecz;
    	double cena=0;
    	for(int i = 0; i<magazyn.size();i++){
    		rzecz = (Item) magazyn.get(i);

    		if(nazwa.contentEquals(rzecz.getNazwa()))//to ta
    		{
    			cena = rzecz.getCena();
    		}
    	}
    	return cena;
    }
    public  ArrayList usunZMag(String nazwa, int ile)
    {
    	Item rzecz;
    	int ilosc;
    	for(int i = 0; i<magazyn.size();i++){
    		rzecz = (Item) magazyn.get(i);

    		if(nazwa.contentEquals(rzecz.getNazwa()))//to ta
    		{
//? usuniecie gdy ilosc 0
    		}
    	
    	}
    	return magazyn;
    	}
    public int cenaCalk( ArrayList kosz){
    	int cenaC=0;
    	Item rzecz;
    	for(int i = 0; i<kosz.size();i++){
    		rzecz = (Item) kosz.get(i);
    		cenaC+=(rzecz.getCena()*rzecz.getIlosc());
    	}
    	return cenaC;
    }
}
