// **********************************************************************
//   Koszyk.java
//
//   Reprezentuje koszyk zakupowy jako tablica przedmiotow (Items)
// **********************************************************************

//import Item;
import java.text.NumberFormat;

public class Koszyk
{

    private int iloscRzeczy;       // ilosc rzeczy w koszyku
    private double cenaCalkowita;  // calkowita cena przedmiotow w koszyku
    private int pojemnosc;         // biezaca pojemnosc koszyka
    private Item[] koszyk;
    // -----------------------------------------------------------
    // tworzy pusty koszyk o pojemnosci 5 przedmiotow.
    // -----------------------------------------------------------
    public Koszyk()
    {
	pojemnosc = 1;
	iloscRzeczy = 0;
	cenaCalkowita = 0.0;
	koszyk = new Item[pojemnosc];
    }

    // -------------------------------------------------------
    //  Dodaje rzecz do koszyka.
    // -------------------------------------------------------
    public void dodajDoKoszyka(String nazwaRzeczy, double cena, int ilosc)
    {
    	Item rzecz = new Item(nazwaRzeczy,cena,ilosc);
    	//System.out.println("met dodawania");

    	if(iloscRzeczy>=koszyk.length)
    		powiekszRozmiar();
    	
    	//System.out.println("dodano do koszyka");// + rzecz.toString());
    	koszyk[iloscRzeczy]=rzecz;
    	iloscRzeczy++;
    	cenaCalkowita+=cena*ilosc;

    }

    // -------------------------------------------------------
    //  Zwraca zawartosc koszyka wraz z podsumowaniem.
    // -------------------------------------------------------
    public String toString()
    {
	NumberFormat fmt = NumberFormat.getCurrencyInstance();

	String zawartosc = "\nKoszyk\n";
	zawartosc += "\nRzecz\t\tCena jednostkowa\tIlosc\tSuma\n";

	for (int i = 0; i < iloscRzeczy; i++)
	    zawartosc += koszyk[i].toString() + "\n";

	zawartosc += "\nTotal Price: " + fmt.format(cenaCalkowita);
	zawartosc += "\n";

	return zawartosc;
    }

    // ---------------------------------------------------------
    //  Zwieksza pojemnosc koszyka o 3
    // ---------------------------------------------------------
    private void powiekszRozmiar()
    {
    	Item[] wiekszyKoszyk = new Item[koszyk.length+3];

    	for(int i =0; i<koszyk.length;i++)
    		wiekszyKoszyk[i]=koszyk[i];
    	
    	System.out.println(wiekszyKoszyk.length);
    	koszyk = wiekszyKoszyk;
   // 	System.out.println("zamienilo: " + koszyk.length); 
    }

}

